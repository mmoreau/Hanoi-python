import sys


class Hanoi:


	def __init__(self):
		pass


	@classmethod
	def Hanoi(cls, n, a, b, c):

		"""Calculates all the possibilities."""

		# ((2 ^ n) - 1) movement


		lock = 0


		if isinstance(n, int):
			if n > 0:
				lock += 1


		if isinstance(a, (int, str)):
			lock += 1


		if isinstance(b, (int, str)):
			lock += 1


		if isinstance(c, (int, str)):
			lock += 1


		if lock == 4:
			Hanoi.Hanoi((n - 1), a, c, b)
			sys.stdout.write(f"{(a, c)}\n")
			Hanoi.Hanoi((n - 1), b, a, c)