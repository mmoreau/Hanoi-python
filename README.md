<h1>Hanoi</h1>

<h3>What are Hanoi Towers ?</h3>
<ul>
    <li>https://en.wikipedia.org/wiki/Tower_of_Hanoi</li>
</ul>

<h2><b>Script</b></h2>
<p>Hanoi.Hanoi(N, A, B, C)</p>
<ul>
    <Li>N : Number of discs</li>
    <li>A : Name of the starting tower</li>
    <li>B : Name of the intermediate tower</li>
    <li>C : Name of the finish tower</li>
</ul>

<h3><b>Example</b></h3>
<p>Display the steps of the 3 discs</p>
<pre>
    <code>
        Hanoi.Hanoi(3, "A", "B", "C")
    </code>
</pre>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>